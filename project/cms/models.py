from django.db import models

# Create your models here.

class Contenidos(models.Model):
    clave = models.CharField(max_length=64)
    valor = models.CharField(max_length=64)
    def __str__(self):
        return str(self.id) + ": " + self.clave + " ---- " + self.valor
    def tiene_as(self):
        return ('a' in self.valor)

class Comentario(models.Model):
    contenido = models.ForeignKey(Contenidos, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=128)
    cuerpo = models.TextField()
    fecha = models.DateTimeField()
    def __str__(self):
        return str(self.id) + ": " + self.titulo
